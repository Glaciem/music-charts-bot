from argparse import ArgumentParser
import time
from dotenv import load_dotenv, find_dotenv

from youtube import Parser
from telega import MyBot

load_dotenv(find_dotenv())


if __name__ == "__main__":

    arg_parser = ArgumentParser()
    arg_parser.add_argument(
        "-t",
        "--time",
        action="store",
        default="7200",
        help="Time for everyday starting. Default 22:00.",
    )
    args = arg_parser.parse_args()

    while True:
        parser = Parser()
        mybot = MyBot()
        new_clips = parser.get()
        mybot.send_clips(new_clips)
        time.sleep(int(args.time))
