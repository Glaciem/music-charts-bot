FROM selenium/standalone-chrome:123.0-chromedriver-123.0

USER root
RUN curl -sS https://bootstrap.pypa.io/get-pip.py | python3

WORKDIR /app
COPY requirements.txt /app
RUN pip3 install -r requirements.txt

COPY . /app