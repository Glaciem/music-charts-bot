from dataclasses import dataclass
import datetime
import time
from selenium.webdriver.common.by import By
from selenium import webdriver
from selenium.webdriver.chromium.options import ChromiumOptions
import logging
import redis
import os
import glob
import pytube
from pytube.streams import Stream
import stat
import shutil
import cv2

from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())

REDIS_TTL = 60 * 60 * 24 * 7 * 3
LOG_FORMAT = "%(asctime)s - [%(levelname)s] - [%(threadName)s] - %(message)s"

logging.basicConfig(
    level=logging.INFO,
    format=LOG_FORMAT,
)


@dataclass
class Clip:
    title: str
    url: str
    video_path: str | None = None
    audio_path: str | None = None
    stream: Stream | None = None
    video_height: int = 0
    video_width: int = 0


class Parser:
    def __init__(
        self,
        page_latency: int = 0,
    ) -> None:
        self.page_latency = page_latency
        self.start_time = datetime.datetime.now()
        self.all_clips: list[Clip] = []
        self.new_clips: list[Clip] = []

        self.redis = redis.StrictRedis(
            os.environ.get("REDIS_URL"),
            port=6379,
            db=1,
            charset="utf-8",
            decode_responses=True,
        )

        options = ChromiumOptions()
        options.add_argument(
            "user-agent=Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0"
        )
        options.add_argument("--disable-blink-features=AutomationControlled")
        options.add_argument("--headless=new")
        options.add_argument("--window-size=1920,1080")
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-dev-shm-usage")
        self.driver = webdriver.Chrome(options=options)

        self.logger = logging.getLogger(__name__)
        formatter = logging.Formatter(LOG_FORMAT)
        file_handler = logging.FileHandler("log.log", mode="a")
        file_handler.setFormatter(formatter)
        self.logger.addHandler(file_handler)

        self.SCROLL_PAUSE_TIME = 1
        self.XPATH_TITLES = '//*[@id="grid-container"]/..//*[@id="video-title"]'
        self.XPATH_MUSIC_BTN = (
            '//*[@id="tabsContent"]/yt-tab-group-shape/div[1]/yt-tab-shape[2]'
        )
        self.MAIN_URL = "https://www.youtube.com/feed/trending/"
        self.driver.get(self.MAIN_URL)
        self._start_latency()

    def _switch_to_music_chart(self):
        self.driver.find_element(By.XPATH, self.XPATH_MUSIC_BTN).click()
        self._start_latency()
        self.logger.info("Switched to music")

    def _scrolling_down(self):
        last_height = self.driver.execute_script("return document.body.scrollHeight")
        while True:
            self.driver.execute_script(
                "window.scrollTo(0, document.body.scrollHeight);"
            )
            time.sleep(self.SCROLL_PAUSE_TIME)
            new_height = self.driver.execute_script("return document.body.scrollHeight")
            if new_height == last_height:
                break
            last_height = new_height
        self.logger.info("Scrolled down")

    def _collect_all_clips_url(self):
        titles = self.driver.find_elements(By.XPATH, self.XPATH_TITLES)
        for title in titles:
            self.all_clips.append(
                Clip(
                    title=title.get_attribute("title"), url=title.get_attribute("href")
                )
            )
        self.logger.info("Collected all clips")

    def _collect_new_clips(self):
        cashed_urls = self.redis.keys("*")
        new_urls: list[Clip] = []
        for clip in self.all_clips:
            if clip.url not in cashed_urls:
                new_urls.append(clip)
        self.logger.info(f"{len(new_urls)} new clips")
        for clip in new_urls:
            self.logger.info(f"download {clip.url}")
            youtube = pytube.YouTube(clip.url)
            video = youtube.streams.get_by_resolution("720p")
            audio = youtube.streams.get_audio_only()
            if not video or not audio:
                self.logger.info(f"fail with {clip.url}")
                continue
            file_name = video.default_filename.replace(".mp4", "")
            if not os.path.exists(f"./downloads/{file_name}"):
                os.makedirs(f"./downloads/{file_name}")
            video.download(f"./downloads/{file_name}", f"{file_name}.mp4")
            audio.download(f"./downloads/{file_name}", f"{file_name}.mp3")
            video_path = f"./downloads/{file_name}/{file_name}.mp4"
            audio_path = f"./downloads/{file_name}/{file_name}.mp3"
            vid = cv2.VideoCapture(video_path)
            height = vid.get(cv2.CAP_PROP_FRAME_HEIGHT)
            width = vid.get(cv2.CAP_PROP_FRAME_WIDTH)
            clip.video_path = video_path
            clip.audio_path = audio_path
            clip.stream = video
            clip.video_height = height
            clip.video_width = width
            self.new_clips.append(clip)
            self.redis.set(clip.url, "", REDIS_TTL)

        self.logger.info(f"{len(self.new_clips)} clips saved")

    @staticmethod
    def clear_downloads():
        folders = glob.glob("./downloads/")
        for folder in folders:
            try:
                shutil.rmtree(folder)
            except:
                logging.error("remove failed", exc_info=True)

    def _start_latency(self):
        if self.page_latency:
            time.sleep(self.page_latency)

    def get(self) -> list[Clip]:
        self.logger.info("START PARSING")
        Parser.clear_downloads()
        self._switch_to_music_chart()
        self._scrolling_down()
        self._collect_all_clips_url()
        self._collect_new_clips()
        self.driver.close()
        return self.new_clips


if __name__ == "__main__":
    parser = Parser()
    print(len(parser.get()))

    # youtube = pytube.YouTube('https://www.youtube.com/watch?v=ujfX2la-CXM')
    # audio = youtube.streams.get_audio_only()
    # video = youtube.streams.get_by_resolution("720p")
    # if not os.path.exists(f'./downloads/{video.iteg}'):
    #     os.makedirs(f'./downloads/{video.itag}')
    # video.download(f'./downloads/{video.itag}', 'video.mp4')
    # audio.download(f'./downloads/{video.itag}', 'audio.mp3')
