import os
from telebot import types, TeleBot
from youtube import Clip
import logging

from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())

API_TOKEN = os.environ.get("TELEGA_API_TOKEN")
CAHT_ID = int(os.environ.get("CAHT_ID"))
LOG_FORMAT = "%(asctime)s - [%(levelname)s] - [%(threadName)s] - %(message)s"

logging.basicConfig(
    level=logging.INFO,
    format=LOG_FORMAT,
)


class MyBot:

    def __init__(self) -> None:
        self.bot = TeleBot(API_TOKEN)

        self.logger = logging.getLogger(__name__)
        formatter = logging.Formatter(LOG_FORMAT)
        file_handler = logging.FileHandler("log.log", mode="a")
        file_handler.setFormatter(formatter)
        self.logger.addHandler(file_handler)

    def send_clips(self, clips: list[Clip]):
        for clip in clips:
            try:
                self.bot.send_video(
                    CAHT_ID,
                    open(clip.video_path, "rb"),
                    supports_streaming=True,
                    caption=f'{clip.title}\n\n<a href="{clip.url}">источник</a>',
                    parse_mode="HTML",
                    height=clip.video_height,
                    width=clip.video_width,
                )
                self.logger.info("Message sent")
            except:
                logging.error("Massage sending failed", exc_info=True)


if __name__ == "__main__":
    mybot = MyBot()
    mybot.send_clips(
        [
            Clip(
                title="TEST",
                url="https://www.youtube.com/watch?v=DiHUEWBRQEI",
                video_path=".\\downloads\\последняя.mp4\\video.mp4",
                audio_path=".\\downloads\\последняя.mp4\\audio.mp3",
                stream=None,
            )
        ]
    )
